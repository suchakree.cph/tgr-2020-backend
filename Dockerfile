FROM node:10.16.1-alpine
RUN rm -rf /var/cache/apk/* && rm -rf /tmp/*
RUN apk update && apk add yarn python g++ make && rm -rf /var/cache/apk/*
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN yarn install
COPY . /usr/src/app
CMD [ "sh", "-c", "yarn start" ]
