import moment from 'moment'
import { connectMQTT } from './src/mqtt'

const getMockData = () => {
  return {
    DevEUI_uplink: {
      Time: moment().format(),
      DevEUI: 'AA00DBCA14EF1406',
      DevAddr: '14EF1406',
      FPort: '2',
      FCntUp: '95',
      ADRbit: '1',
      MType: '4',
      FCntDn: '95',
      payload_hex: '00000000000000fe000000',
      mic_hex: '8b7a19d2',
      Lrcid: '00000231',
      LrrRSSI: '-111.000000',
      LrrSNR: '5.000000',
      SpFact: '7',
      SubBand: 'G1',
      Channel: 'LC15',
      DevLrrCnt: '1',
      Lrrid: '1000018F',
      Late: '0',
      LrrLAT: '19.027563',
      LrrLON: '99.891350',
      Lrrs: {
        Lrr: [
          {
            Lrrid: '1000018F',
            Chain: '0',
            LrrRSSI: '-111.000000',
            LrrSNR: '5.000000',
            LrrESP: '-112.193314'
          }
        ]
      },
      CustomerID: '1100001293',
      CustomerData: { alr: { pro: 'LORA/Generic', ver: '1' } },
      ModelCfg: '0',
      InstantPER: '0.000000',
      MeanPER: '0.000000'
    }
  }
}

const {
  MQTT_BROKER_URL,
  MQTT_TOPIC_PM25,
  MQTT_TOPIC_TRACK,
  TEAM_ID
} = process.env

const handleMessage = (topic, message) => {
  console.log(`New message from topic : ${topic}\n ${message}`)
}

connectMQTT({ brokerURL: MQTT_BROKER_URL, clientId: 'mock-tgr06' })
  .then(connection => {
    console.log('MQTT connected')
    connection.on('message', handleMessage)
    connection.subscribe(MQTT_TOPIC_PM25)
    connection.subscribe(MQTT_TOPIC_TRACK)

    setInterval(() => {
      console.log(`Sending message to tgr2020/pm25/data/${TEAM_ID}`)
      connection.publish(
        `tgr2020/pm25/data/${TEAM_ID}`,
        JSON.stringify(getMockData())
      )
      console.log(`Sending message to tgr2020/track/data/${TEAM_ID}`)
      connection.publish(
        `tgr2020/track/data/${TEAM_ID}`,
        JSON.stringify(getMockData())
      )
    }, 10000)
  })
  .catch(err => {
    console.error(`MQTT connection error ${err}`)
  })
