import socketIOClient from 'socket.io-client'
import readLine from 'readline'

const { API_PORT } = process.env

const socket = socketIOClient(`http://localhost:${API_PORT}`)

socket.on('connect', () => {
  console.log('connected')
})

socket.on('test', message => {
  console.log(`Message: ${message}`)
})

const lineReader = readLine.createInterface({
  input: process.stdin,
  output: process.stdout
})

const yay = async () => {
  while (true) {
    for await (const line of lineReader) {
      socket.emit('test', line)
    }
  }
}

yay()
