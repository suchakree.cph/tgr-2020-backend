import express from 'express'
import bodyParser from 'body-parser'
import route from './routes'
import { connectDB } from './models'
import io from 'socket.io'
import http from 'http'
import './controllers/mqtt'
import './services'
import { initSocketIO } from './socket'
import cors from 'cors'

const API_PORT = process.env.API_PORT || 8000

const {
  DB_HOST,
  DB_USER,
  DB_PASS,
  DB_DATABASE,
  DB_AUTH_DATABASE,
  DB_POOL_SIZE
} = process.env

connectDB({
  dbHost: DB_HOST,
  dbUser: DB_USER,
  dbPass: DB_PASS,
  dbDatabase: DB_DATABASE,
  authdb: DB_AUTH_DATABASE,
  poolSize: DB_POOL_SIZE
})
  .then(dbConn => {
    const app = express()

    app.use(cors())

    app.use(bodyParser.json())

    app.use(route)

    const _http = http.createServer(app)

    initSocketIO(_http)

    _http.listen(API_PORT, () => {
      console.log(`Server started at port ${API_PORT}`)
    })
  })
  .catch(err => {
    console.error(err)
  })
