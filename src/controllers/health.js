import { emitMessage } from '../socket'

const { TEAM_NAME } = process.env

export const checkHealth = (req, res) => {
  res.send(TEAM_NAME)
}

export const emitIO = (req, res) => {
  emitMessage('test', req.params.msg)
  res.status(200).end()
}

export default {
  checkHealth,
  emitIO
}
