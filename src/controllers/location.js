import Sensors from '../models/sensors'
import { SENSOR_TYPE } from '../enum/sensor'

const { PM25_NODE_IDS } = process.env

const pm25NodeIDs = PM25_NODE_IDS.split(',')

export const listPM25Node = async (req, res) => {
  try {
    const nodes = await Sensors.aggregate([
      {
        $match: {
          sensorType: SENSOR_TYPE.PM25,
          sensorID: {
            $in: pm25NodeIDs
          }
        }
      },
      {
        $sort: {
          ts: -1
        }
      },
      {
        $group: {
          _id: '$sensorID',
          pm25: { $first: '$pm25' },
          location: { $first: '$location' },
          ts: { $first: '$ts' }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ])

    res.json({
      success: true,
      data: nodes
    })
  } catch (err) {
    console.error(err)
    res.status(500).json({
      success: false
    })
  }
}

export default {
  listPM25Node
}
