import Sensors from '../models/sensors'
import HTTPStatus from 'http-status-codes'

export const getRequestRPM = async (req, res) => {
  try {
    const data = await Sensors.aggregate([
      {
        $group: {
          _id: {
            $dateToString: { format: '%Y-%m-%dT%H:%M', date: '$createdAt' }
          },
          count: { $sum: 1 }
        }
      },
      {
        $project: {
          _id: 0,
          date: '$_id',
          count: 1
        }
      }
    ])

    res.json({
      success: true,
      data
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
      success: false
    })
  }
}

export default {
  getRequestRPM
}
