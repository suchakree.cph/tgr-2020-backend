import { connectMQTT } from '../mqtt'
import { addRawSensorData } from './sensor'
import { SENSOR_TYPE } from '../enum/sensor'

const {
  MQTT_BROKER_URL,
  MQTT_TOPIC_PM25,
  MQTT_TOPIC_TRACK,
  MQTT_CLIENT_ID
} = process.env

const handleMessage = async (topic, message) => {
  try {
    console.log(`New message from topic : ${topic}`)

    if (!topic.match(/tgr2020\/.+/g)) {
      return console.log(`${topic} not match regex ignoring`)
    }

    let sensorType, sensorID

    const splitedTopic = topic.split('/')
    sensorID = splitedTopic[splitedTopic.length - 1]

    if (topic.match(/tgr2020\/track.+/g)) {
      sensorType = SENSOR_TYPE.TRACK
    }

    if (topic.match(/tgr2020\/pm25.+/g)) {
      sensorType = SENSOR_TYPE.PM25
    }

    const data = JSON.parse(message)

    if (sensorType === SENSOR_TYPE.PM25 && !data.DevEUI_uplink) {
      return console.log(
        `data not contain DevEUI_uplink ignoring\ndata : ${message}`
      )
    }

    await addRawSensorData({ sensorType, sensorID, data })
  } catch (err) {
    console.error(err)
  }
}

connectMQTT({ brokerURL: MQTT_BROKER_URL, clientId: MQTT_CLIENT_ID })
  .then(connection => {
    console.log('MQTT connected')
    connection.on('message', handleMessage)
    connection.subscribe(MQTT_TOPIC_PM25)
    connection.subscribe(MQTT_TOPIC_TRACK)
  })
  .catch(err => {
    console.error(`MQTT connection error ${err}`)
  })
