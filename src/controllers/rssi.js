import Sensors from '../models/sensors'
import { SENSOR_TYPE } from '../enum/sensor'
import moment from 'moment'

const { TEAM_MAC_ADDR, GATEWAY_CHANNEL } = process.env

const gatewayChannel = GATEWAY_CHANNEL.split(',')

const fillValue = -89

const _getRSSIByMAC = async macAddr => {
  const sensorData = await Sensors.aggregate([
    {
      $match: {
        sensorType: SENSOR_TYPE.TRACK,
        macAddr,
        sensorID: { $in: gatewayChannel },
        createdAt: {
          $gte: moment()
            .subtract(3, 'minute')
            .toDate()
        }
      }
    },
    {
      $sort: {
        createdAt: -1
      }
    },
    {
      $group: {
        _id: '$sensorID',
        rssi: { $first: '$rssi' }
      }
    }
  ])

  if (!sensorData.length) return null

  const data = {}

  sensorData.forEach(s => {
    const channel = s._id
    data[channel] = s.rssi
  })

  return data
}

export const getRSSIByMAC = async (req, res) => {
  try {
    const macAddr = req.query
    const sensorData = await _getRSSIByMAC(macAddr)

    const orderedData = gatewayChannel.map(c => {
      return sensorData[c] || fillValue
    })

    res.json({
      success: true,
      data: {
        orderedData
      }
    })
  } catch (err) {
    console.error(err)
    res.status(500).json({
      success: false
    })
  }
}

export const getTeamRSSI = async (req, res) => {
  try {
    const sensorData = await _getRSSIByMAC(TEAM_MAC_ADDR)

    if (!sensorData) {
      return res.json({
        success: true,
        data: {
          rssi: [fillValue, fillValue, fillValue, fillValue]
        }
      })
    }

    const orderedData = gatewayChannel.map(c => {
      return sensorData[c] || fillValue
    })

    res.json({
      success: true,
      data: {
        rssi: orderedData
      }
    })
  } catch (err) {
    console.error(err)
    res.status(500).json({
      success: false
    })
  }
}

export default {
  getRSSIByMAC,
  getTeamRSSI
}
