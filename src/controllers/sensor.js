import Sensors from '../models/sensors'
import moment from 'moment'
import HTTPStatus from 'http-status-codes'
import { SENSOR_TYPE } from '../enum/sensor'
import RawSensor from '../models/rawSensor'

const { TEAM_ID } = process.env

export const addRawSensorData = async ({ sensorType, sensorID, data }) => {
  await RawSensor.create({
    sensorType,
    sensorID,
    data
  })
}

export const _listSensorData = async ({
  sensorType,
  start,
  end,
  skip,
  limit
}) => {
  const matchQuery = {
    sensorType,
    ts: {
      $gte: start,
      $lt: end
    }
  }
  const data = await Sensors.find(matchQuery)
    .sort({ ts: -1 })
    .skip(skip)
    .limit(limit)
    .lean()
    .exec()

  const count = await Sensors.countDocuments(matchQuery)

  return { data, count }
}

export const _getLastestSensorDataByDeviceID = async ({
  deviceID,
  sensorType
}) => {
  const data = await Sensors.find({
    sensorType,
    deviceID
  })
    .sort({ ts: -1 })
    .limit(1)
    .lean()
    .exec()

  return data.length === 0 ? null : data[0]
}

export const _getLastestSensorDataBySensorID = async ({
  sensorID,
  sensorType
}) => {
  const data = await Sensors.find({
    sensorType,
    sensorID
  })
    .sort({ ts: -1 })
    .limit(1)
    .lean()
    .exec()

  return data.length === 0 ? null : data[0]
}

export const getLastestPM25OwnTeam = async (req, res) => {
  try {
    const data = await _getLastestSensorDataByDeviceID({
      deviceID: TEAM_ID,
      sensorType: SENSOR_TYPE.PM25
    })
    res.json({
      success: true,
      data
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({ success: false })
  }
}

export const listPM25 = async (req, res) => {
  try {
    let { start, end, skip, limit } = req.query

    start = moment(start).toDate()
    end = moment(end).toDate()

    skip = parseInt(skip, 10)
    limit = parseInt(limit, 10)

    const { data, count } = await _listSensorData({
      sensorType: SENSOR_TYPE.PM25,
      start,
      end,
      skip,
      limit
    })

    res.json({
      success: true,
      data,
      count
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
      success: false
    })
  }
}

export const getLatestPM25ByDeviceID = async (req, res) => {
  try {
    const { deviceID } = req.params

    const data = await _getLastestSensorDataByDeviceID({
      deviceID,
      sensorType: SENSOR_TYPE.PM25
    })

    res.json({
      success: true,
      data
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
      success: false
    })
  }
}

export const listTracking = async (req, res) => {
  try {
    let { start, end, skip, limit } = req.query

    start = moment(start).toDate()
    end = moment(end).toDate()

    skip = parseInt(skip, 10)
    limit = parseInt(limit, 10)

    const { data, count } = await _listSensorData({
      sensorType: SENSOR_TYPE.TRACK,
      start,
      end,
      skip,
      limit
    })

    res.json({
      success: true,
      data,
      count
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
      success: false
    })
  }
}

export const getLatestTrackingBySensorID = async (req, res) => {
  try {
    const { sensorID } = req.params

    const data = await _getLastestSensorDataBySensorID({
      sensorID,
      sensorType: SENSOR_TYPE.TRACK
    })

    res.json({
      success: true,
      data
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({
      success: false
    })
  }
}

export const getLastestTrackingOwnTeam = async (req, res) => {
  try {
    const data = await _getLastestSensorDataBySensorID({
      sensorID: TEAM_ID,
      sensorType: SENSOR_TYPE.TRACK
    })

    res.json({
      success: true,
      data
    })
  } catch (err) {
    console.error(err)
    res.status(HTTPStatus.INTERNAL_SERVER_ERROR).json({ success: false })
  }
}

export default {
  listPM25,
  listTracking,
  getLatestPM25ByDeviceID,
  getLatestTrackingBySensorID,
  getLastestPM25OwnTeam,
  getLastestTrackingOwnTeam
}
