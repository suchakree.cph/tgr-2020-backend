import mongoose from 'mongoose'

export const connectDB = ({
  dbHost,
  dbUser,
  dbPass,
  dbDatabase,
  poolSize,
  authdb
}) => {
  return new Promise((resolve, reject) => {
    let mongoDBURL

    const connCfg = {
      poolSize: poolSize,
      auth: null,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }

    if (dbUser) {
      mongoDBURL = `mongodb://${dbUser}:${dbPass}@${dbHost}/${dbDatabase}`
      connCfg.auth = { authdb: authdb }
    } else {
      mongoDBURL = `mongodb://${dbHost}/${dbDatabase}`
    }

    mongoose.connect(mongoDBURL, connCfg)

    const db = mongoose.connection

    db.on('error', err => {
      reject(err)
    })

    db.once('open', () => {
      resolve(db)
    })
  })
}
