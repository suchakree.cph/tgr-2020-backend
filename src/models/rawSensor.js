import mongoose, { Schema } from 'mongoose'

const rawSensorsSchema = new Schema(
  {
    sensorType: String,
    sensorID: String,
    data: {},
    cleaned: {
      type: Boolean,
      default: false,
      index: true
    }
  },
  { timestamps: true }
)

const RawSensor = mongoose.model('raw-sensors', rawSensorsSchema)

export default RawSensor
