import mongoose, { Schema } from 'mongoose'

const sensorsSchema = new Schema(
  {
    sensorType: {
      type: String,
      index: true
    },
    sensorID: {
      type: String,
      index: true
    },
    deviceID: {
      type: String,
      index: true
    },
    pm25: Number,
    location: {
      lat: Number,
      lng: Number
    },
    rssi: Number,
    macAddr: {
      type: String,
      index: true
    },
    eventCode: Number,
    ts: {
      type: Date,
      index: true
    }
  },
  { timestamps: true }
)

const Sensors = mongoose.model('sensors', sensorsSchema)

export default Sensors
