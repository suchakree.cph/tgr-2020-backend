import mqtt from 'mqtt'

const { TEAM_NAME } = process.env

let connection

// timeout in second
export const connectMQTT = ({
  brokerURL,
  clientId = TEAM_NAME,
  timeout = 10
}) => {
  return new Promise((resolve, reject) => {
    if (connection) {
      return resolve(connection)
    }

    const timer = setTimeout(() => {
      reject('timeout')
    }, timeout * 1000)

    connection = mqtt.connect(brokerURL, { clientId })

    connection.on('connect', () => {
      clearTimeout(timer)
      resolve(connection)
    })
  })
}
