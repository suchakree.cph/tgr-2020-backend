import { Router } from 'express'
import Health from '../controllers/health'

const router = Router()

router.get('/', Health.checkHealth)
router.get('/:msg', Health.emitIO)

export default router
