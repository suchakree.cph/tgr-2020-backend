import { Router } from 'express'
import Health from './health'
import Monitor from './monitor'
import Sensor from './sensor'
import RSSI from './rssi'
import Location from './location'

const router = Router()

router.use('/health', Health)
router.use('/monitor', Monitor)
router.use('/sensor', Sensor)
router.use('/rssi', RSSI)
router.use('/location', Location)

export default router
