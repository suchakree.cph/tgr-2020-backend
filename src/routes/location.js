import { Router } from 'express'
import Location from '../controllers/location'

const router = Router()

router.get('/', Location.listPM25Node)

export default router
