import { Router } from 'express'
import Monitor from '../controllers/monitor'
const router = Router()

router.get('/rpm', Monitor.getRequestRPM)

export default router
