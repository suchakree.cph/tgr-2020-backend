import { Router } from 'express'
import RSSI from '../controllers/rssi'

const router = Router()

router.get('/', RSSI.getTeamRSSI)
router.get('/:macAddr', RSSI.getRSSIByMAC)

export default router
