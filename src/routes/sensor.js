import { Router } from 'express'
import Sensor from '../controllers/sensor'

const router = Router()

router.get('/pm25_data/list', Sensor.listPM25)
router.get('/pm25_data/test', Sensor.getLastestPM25OwnTeam)
router.get('/pm25_data/:deviceID', Sensor.getLatestPM25ByDeviceID)

router.get('/track_data/list', Sensor.listTracking)
router.get('/track_data/test', Sensor.getLastestTrackingOwnTeam)
router.get('/track_data/:sensorID', Sensor.getLatestTrackingBySensorID)

export default router
