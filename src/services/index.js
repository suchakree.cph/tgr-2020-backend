import { cleanSensorData } from './sensor'

const registeredList = []

export const registerService = ({ handler, interval = 1000 }) => {
  const serivce = setInterval(handler, interval)
  registeredList.push(serivce)
}

registerService({ handler: cleanSensorData })
