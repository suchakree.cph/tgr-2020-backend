import RawSensor from '../models/rawSensor'
import Sensors from '../models/sensors'
import { SENSOR_TYPE } from '../enum/sensor'
import { emitMessage } from '../socket'

const { TEAM_MAC_ADDR } = process.env

export const ACCIDENT_ID = {
  8: 'unconscious',
  128: 'fall',
  255: 'slip'
}

const decodePM25 = hex => {
  const pm25 = parseInt(hex.substr(2, 2), 16) || 191
  return pm25
}

const detectTeamSensorChange = sensors => {
  let rssiChange = false
  let accidentEventID = 0

  for (let i = 0; i < sensors.length; i++) {
    if (sensors[i].sensorType === SENSOR_TYPE.TRACK) {
      if (sensors[i].macAddr === TEAM_MAC_ADDR) {
        rssiChange = true
        if (sensors[i].eventCode) {
          accidentEventID = sensors[i].eventCode
        }
        console.log('Found TEAM MAC emiting socket.io')
      }
    }
  }

  if (accidentEventID) {
    console.log('Accident detected')
    emitMessage('accident', ACCIDENT_ID[accidentEventID])
    return
  }

  if (rssiChange) {
    console.log('RSSI change detected')
    emitMessage('rssi', accidentEventID.toString())
  }
}

export const cleanSensorData = async () => {
  const data = await RawSensor.find({ cleaned: false }).lean()

  const filteredData = data.filter(d => {
    if (d.sensorType === SENSOR_TYPE.PM25) {
      if (!d.data.DevEUI_uplink) {
        return false
      }
      if (!d.data.DevEUI_uplink.LrrLAT || !d.data.DevEUI_uplink.LrrLON) {
        return false
      }
      if (!d.data.DevEUI_uplink.payload_hex) {
        return false
      } else {
        const pm25 = decodePM25(d.data.DevEUI_uplink.payload_hex)
        if (pm25 === 191) {
          return false
        }
      }

      return true
    }
    if (d.sensorType === SENSOR_TYPE.TRACK) {
      if (!d.data.mac_addr || !d.data.rssi) {
        return false
      }
      return true
    }

    return false
  })

  const mappedData = filteredData.map(d => {
    const { DevEUI_uplink } = d.data

    if (d.sensorType === SENSOR_TYPE.PM25) {
      return {
        sensorType: d.sensorType,
        deviceID: d.sensorID,
        ts: DevEUI_uplink.Time,
        sensorID: d.sensorID,
        pm25: DevEUI_uplink.payload_hex,
        location: {
          lat: DevEUI_uplink.LrrLAT,
          lng: DevEUI_uplink.LrrLON
        },
        rssi: d.LrrRSSI,
        pm25: decodePM25(DevEUI_uplink.payload_hex)
      }
    }
    if (d.sensorType == SENSOR_TYPE.TRACK) {
      return {
        sensorType: d.sensorType,
        deviceID: d.sensorID,
        sensorID: d.sensorID,
        macAddr: d.data.mac_addr,
        rssi: d.data.rssi,
        eventCode: d.data.event_code,
        ts: d.timestamp || new Date()
      }
    }
  })

  await Sensors.create(mappedData)

  const rawSenserID = data.map(d => d._id)

  await RawSensor.updateMany({ _id: { $in: rawSenserID } }, { cleaned: true })

  detectTeamSensorChange(mappedData)
}
