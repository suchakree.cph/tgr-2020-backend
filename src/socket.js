import socketIO from 'socket.io'

let io

export const initSocketIO = http => {
  io = socketIO(http)
  io.on('connection', () => {
    console.log('New user connected')
  })
}

export const emitMessage = (event, msg) => {
  io.emit(event, msg)
}
