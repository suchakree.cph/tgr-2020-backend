import { connectDB } from '../src/models'
import { connectMQTT } from '../src/mqtt'

const {
  DB_HOST,
  DB_USER,
  DB_PASS,
  DB_DATABASE,
  DB_AUTH_DATABASE,
  DB_POOL_SIZE,
  MQTT_BROKER_URL
} = process.env

const testMongoConnection = () => {
  return new Promise(resolve => {
    connectDB({
      dbHost: DB_HOST,
      dbUser: DB_USER,
      dbPass: DB_PASS,
      dbDatabase: DB_DATABASE,
      authdb: DB_AUTH_DATABASE,
      poolSize: DB_POOL_SIZE
    }).then(dbConn => {
      console.log('MongoDB connected')

      console.log(`Connection state: ${dbConn.readyState}`)

      resolve()
    })
  })
}

const testMQTTConnection = () => {
  return new Promise(resolve => {
    connectMQTT({
      brokerURL: MQTT_BROKER_URL
    }).then(connection => {
      console.log('- test mqtt echo')

      connection.on('message', (topic, message) => {
        console.log(`--- message from topic ${topic}: ${message.toString()}`)
        resolve()
      })

      connection.subscribe('echo', err => {
        if (err) {
          console.error('-- error subscribe echo topic')
          resolve()
        }
        console.log('-- subscribed echo topic')
        connection.publish('echo', 'This is message')
      })
    })
  })
}

const runTest = async () => {
  console.log('Test mongo connection')
  await testMongoConnection()

  console.log('\n-------------------\n')

  console.log('Test MQTT connection')
  await testMQTTConnection()

  console.log('\n-------------------\n')

  console.log('Finished all test')
}

runTest()
